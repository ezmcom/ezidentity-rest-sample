# EzIdentity REST Sample
A REST client JAVA sample project demonstrates how to call EzIdentity REST API and process the response.

To build the project, please execute the following command:

```mvn clean install```

