package com.ezmcom.ezidentity.rest.client.signup;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit class to demonstrate and test to call REST API to create an Activate QR Code in response.
 */
public class RestClientCreateActivateQRTest {

    RestClientCreateActivateQR target = null;
    String restURLEndpoint = "https://<HOSTNAME>/ezwsrest"; // change the <HOSTNAME> to the proper REST domain URL 
    String resource = "/rest/engine/softtk/s/regeztoken"; // must start with a '/' char
    String actionBy = "RestClientTest";
    String samlId = "938B07ACA533775A3AFDAA710C87B30E";
    String userId = "user@example.com";
    String contactType = "2";
    String contactDetail = "user@example.com";
    
    @Before
    public void before() {
        target = new RestClientCreateActivateQR(restURLEndpoint);
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void testExecuteCall() {
        try {
            // Construct the Form POST parameters
            List params = new ArrayList();
            params.add(new BasicNameValuePair("actionBy", actionBy));
            params.add(new BasicNameValuePair("samlId", samlId));
            params.add(new BasicNameValuePair("userId", userId));
            params.add(new BasicNameValuePair("contactType", contactType));
            params.add(new BasicNameValuePair("contactDetail", contactDetail));
            
            CreateActivateQRJSONResponse resp = (CreateActivateQRJSONResponse) target.executeCall(resource, params);
            
            // test object not null
            Assert.assertNotNull(resp);
            
            // test return code not null
            Assert.assertNotNull(resp.getReturnCode()); 
            
            // test return code is Success or Token Quota Reach, if Token Quota Reach, call to delete account
            Assert.assertTrue(resp.getReturnCode().intValue() == 0 || resp.getReturnCode().intValue() == -28);
            
            // test credential not null
            Assert.assertNotNull(resp.getCredential());
            
            // test Base64-encoded QRCode image is not null
            Assert.assertNotNull(resp.getEncodedQRCodeImg());
        }
        catch(Exception ex) {
            throw new AssertionError("Error in testing RestClientCreateActivateQR.executeCall", ex);
        }
    }
    
}
