package com.ezmcom.ezidentity.rest.client.signup;

import java.io.Serializable;

/**
 * Class used to represent the JSON response from REST API:
 * /rest/engine/softtk/s/regeztoken
 * 
 * Sample JSON: 
 * {
 *   "returnCode":0,
 *   "returnMessage":"Success",
 *   "credential":"JQ7DqBKymXvQljFJcDlXlpUrw",
 *   "encodedQRCodeImg": "iVBORw0KGgoAAAANS......"
 * }
 * 
 */
public class CreateActivateQRJSONResponse implements Serializable {

    private static final long serialVersionUID = 5806731930777142677L;
    
    private Integer returnCode;
    private String returnMessage;
    private String credential;
    private String encodedQRCodeImg;
    
    public Integer getReturnCode() {
        return returnCode;
    }
    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }
    public String getReturnMessage() {
        return returnMessage;
    }
    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }
    public String getCredential() {
        return credential;
    }
    public void setCredential(String credential) {
        this.credential = credential;
    }
    public String getEncodedQRCodeImg() {
        return encodedQRCodeImg;
    }
    public void setEncodedQRCodeImg(String encodedQRCodeImg) {
        this.encodedQRCodeImg = encodedQRCodeImg;
    }

}
