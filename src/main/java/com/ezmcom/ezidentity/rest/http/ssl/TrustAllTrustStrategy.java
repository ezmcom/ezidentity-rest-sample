package com.ezmcom.ezidentity.rest.http.ssl;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import org.apache.http.ssl.TrustStrategy;

/**
 * {@link TrustStrategy} implementation class that will trust all SSL certificate.
 */
public class TrustAllTrustStrategy implements TrustStrategy {

    public boolean isTrusted(X509Certificate[] ax509certificate, String s) throws CertificateException {        
        return true; // Return always trust
    }

}
