package com.ezmcom.ezidentity.rest.client.signup;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;

import com.ezmcom.ezidentity.rest.client.RestClient;
import com.ezmcom.ezidentity.rest.http.HttpClientBuilderProvider;
import com.google.gson.Gson;

/**
 * REST client class to create an Activate QR Code in response
 */
public class RestClientCreateActivateQR extends RestClient {

    public RestClientCreateActivateQR(String restURLEndpoint) {
        super(restURLEndpoint);
    }

    @Override
    public CreateActivateQRJSONResponse executeCall(String resource, UrlEncodedFormEntity formEntity) throws Exception {
        CloseableHttpClient httpClient = null;
        CreateActivateQRJSONResponse jsonResp = null;
        try {
            // Construct full HTTP URL
            String url = restURLEndpoint+resource;
            
            // Create CloseableHttpClient instance
            httpClient = HttpClientBuilderProvider.getBuilder(url, true).build();
            
            // Prepare HttpPost instance
            HttpPost post = new HttpPost(url);
            
            // Add HTTP request header to be application/x-www-form-urlencoded
            post.addHeader(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");
            
            // Set the Form post
            post.setEntity(formEntity);
            
            // Establish the REST HTTP call
            HttpResponse resp = httpClient.execute(post);
            
            // Get HTTP response status
            if( resp.getStatusLine() == null ) {
                throw new Exception("HttpClient call status is null");
            }
            
            // Check the HTTP response status code must be 200
            int statusCode = resp.getStatusLine().getStatusCode();
            if( statusCode != HttpStatus.SC_OK ) {
                throw new Exception("HttpClient response status is not " + HttpStatus.SC_OK);
            }
            
            // Get the HTTP response body
            HttpEntity entity = resp.getEntity();
            if( entity == null ) {
                throw new Exception("HttpClient response entity is null");
            }
            
            // Process and read the HTTP response body into a JSON string
            String json = null;
            InputStream is = null;
            try {
                is = entity.getContent();
                byte[] content = null;
                ByteArrayOutputStream baos = null;
                try {
                    baos = new ByteArrayOutputStream();
                    int byteRead = 0;
                    byte[] buffer = new byte[1024];
                    while( (byteRead = is.read(buffer)) != -1 ) {
                        baos.write(buffer, 0, byteRead);
                    }
                    content = baos.toByteArray();
                }
                finally {
                    if( baos != null ) try { baos.close(); } catch(Throwable th) {}
                }
                
                if( content == null || content.length == 0 ) {
                    throw new Exception("HttpClient response content is null or empty");
                }
                
                json = new String(content, "UTF-8");
            }
            finally {
                if( is != null ) try { is.close(); } catch(Throwable th) {}
            }
            
            // Convert JSON string to the mapped class object
            try {
                jsonResp = new Gson().fromJson(json, CreateActivateQRJSONResponse.class);
            }
            catch(Throwable th) {
                throw new Exception("HttpClient response content is not a valid JSON represented the class "
                        + CreateActivateQRJSONResponse.class.getName());
            }
        }
        finally {
            // Must close the CloseableHttpClient connection
            if( httpClient != null ) try { httpClient.close(); } catch(Throwable th) {}
        }
        
        return jsonResp;
    }

}
