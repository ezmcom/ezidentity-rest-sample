package com.ezmcom.ezidentity.rest.http;

import java.net.URI;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;

import org.apache.http.HttpHost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;

import com.ezmcom.ezidentity.rest.http.ssl.TrustAllTrustStrategy;

/**
 * A helper class to help to create the {@link HttpClientBuilder} instance, where later can use it 
 * for getting the {@link HttpClients} instance from API: {@link HttpClientBuilder#build()}.
 */
public class HttpClientBuilderProvider {
    
    public static HttpClientBuilder getBuilder(String url) throws Exception {
        return getBuilder(URI.create(url), false);
    }
    
    public static HttpClientBuilder getBuilder(String url, boolean trustAllSSL) throws Exception {
        return getBuilder(URI.create(url), trustAllSSL);
    }
    
    public static HttpClientBuilder getBuilder(String url, HttpHost proxy) throws Exception {
        return getBuilder(URI.create(url), proxy);
    }
    
    /**
     * Construct {@link HttpClientBuilder} instance.
     * 
     * @param url HTTP URL to be connected. Provide full HTTP URL with scheme http:// or https://.
     * @param trustAllSSL Trust all SSL certificate. Only applicable if the URL is https.
     * @param proxy {@link HttpHost} instance of proxy. Provide null, if proxy is not applicable.
     */
    public static HttpClientBuilder getBuilder(String url, boolean trustAllSSL, HttpHost proxy) throws Exception {
        return getBuilder(URI.create(url), trustAllSSL, proxy);
    }
    
    public static HttpClientBuilder getBuilder(URI uri) throws Exception {
        return getBuilder(uri, false);
    }
    
    public static HttpClientBuilder getBuilder(URI uri, boolean trustAllSSL) throws Exception {
        return getBuilder(uri, trustAllSSL, null);
    }
    
    public static HttpClientBuilder getBuilder(URI uri, HttpHost proxy) throws Exception {
        return getBuilder(uri, false, proxy);
    }
    
    /**
     * Construct {@link HttpClientBuilder} instance.
     * 
     * @param uri {@link URI} instance that contains the HTTP URL to be connected.
     * @param trustAllSSL Trust all SSL certificate. Only applicable if the URL is https.
     * @param proxy {@link HttpHost} instance of proxy. Provide null, if proxy is not applicable.
     */
    public static HttpClientBuilder getBuilder(URI uri, boolean trustAllSSL, HttpHost proxy) throws Exception {
        if( uri == null || uri.getScheme() == null ) {
            throw new IllegalArgumentException("Param uri is null or no schema");
        }
        
        HttpClientBuilder builder = null;
        if( uri.getScheme().equalsIgnoreCase("https") && trustAllSSL ) {
            builder = createTrustAllSSLHttpClient();
        }
        else {
            builder = HttpClientBuilder.create();
        }
        
        if( proxy != null ) {
            builder.setProxy(proxy);
        }
        
        return builder;
    }
    
    private static HttpClientBuilder createTrustAllSSLHttpClient() throws Exception {
        HostnameVerifier hostnameVerifier = new NoopHostnameVerifier();
        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(new TrustAllTrustStrategy()).build();
        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, hostnameVerifier);
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", sslSocketFactory)
                .build();
        PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        
        HttpClientBuilder builder = HttpClients.custom();
        builder.setSSLHostnameVerifier(hostnameVerifier);
        builder.setSSLContext(sslContext);
        builder.setSSLSocketFactory(sslSocketFactory);
        builder.setConnectionManager(connMgr);
        return builder;
    }
    
}
